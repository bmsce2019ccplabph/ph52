#include<stdio.h>

int main()
{
    FILE *fa;
    char c;
    fa=fopen("input.txt","w");
    printf("Enter input data and then type ctrl-d\n");
    while((c=getchar())!=EOF)
    {
        putc(c,fa);
    }
    fclose(fa);
    fa=fopen("input.txt","r");
    while((c=getc(fa))!=EOF)
    {
        printf("%c",c);
    }
    fclose(fa);
    return 0;
    
}