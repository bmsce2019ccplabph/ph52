#include <stdio.h>

int max(int h,int g,int k)
{ 
    int max;
    max=(h>g?(h>k?h:k):(g>k?g:k));
    return max;
}

int scan(int a)
{
    scanf("%d",&a);
    return a;
}

void print(int a)
{
    printf("%d\n",a);
}

int main()
{
    int x,y,z,f;
    printf("enter three numbers\n");
    scanf("%d %d %d",&x,&y,&z);
    f=max(x,y,z);
    printf("max=");
    print(f);
    return 0;
}