#include <stdio.h>

int sum(int a)
{
	int n,sum=0;
	do
	{
		n=a%10;
		sum=sum+n;
		a=a/10;
	}while(n!=0);
	return sum;
}

void print(int a,int b)
{
	printf("the sum of digits of %d=%d\n",a,b);
}


int main()
{
	int a,b;
	printf("enter the number here\n");
	scanf("%d",&a);
	b=sum(a);
	print(a,b);
	return 0;	
}